# Example configs for various t00ls , swiss knife style

- `supervisord-socat-mailrelay/` →  forward mail connection with custom ssl certificate  ( e.g. to provide any.host via dedicated ip → beware to use the logs , since your real server sees proxy ip)
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/example-configs/README.md/logo.jpg" width="480" height="270"/></div></a>
